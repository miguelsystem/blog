json.array!(@commets) do |commet|
  json.extract! commet, :id, :post, :body
  json.url commet_url(commet, format: :json)
end
